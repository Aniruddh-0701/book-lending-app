package com.example.lendmebook

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.EditText

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        this.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        display()
    }

    private fun display() {
        val name = findViewById<EditText>(R.id.editTextTextPersonName)
        val phoneNumber = findViewById<EditText>(R.id.editTextPhone)
        val emailId = findViewById<EditText>(R.id.editTextTextEmailAddress)
        val bookName = findViewById<EditText>(R.id.editTextTextBookName)
        val submit = findViewById<Button>(R.id.submitButton)

        submit.isEnabled =
            (!(name.text == null || phoneNumber.text == null || emailId.text == null || bookName.text == null) && (name.text.isNotEmpty() && phoneNumber.text.isNotEmpty() && emailId.text.isNotEmpty() && bookName.text.isNotEmpty()))
    }

    fun reset(view: View?) {
        findViewById<EditText>(R.id.editTextTextPersonName).setText("")
        findViewById<EditText>(R.id.editTextPhone).setText("")
        findViewById<EditText>(R.id.editTextTextEmailAddress).setText("")
        findViewById<EditText>(R.id.editTextTextBookName).setText("")
        this.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
    }

    fun submitForm(view: View) {
        val text = requestMessage
        val intent = Intent(Intent.ACTION_SENDTO)
        intent.data = Uri.parse("mailto:") // only email apps should handle this
        intent.putExtra(Intent.EXTRA_SUBJECT, text[0])
        intent.putExtra(Intent.EXTRA_TEXT, text[1])
        if (intent.resolveActivity(packageManager) != null) {
            startActivity(intent)
        }
        // Finish the order
        Companion.requestMessage = text[1]
        reset(view)
    }

    private val requestMessage: Array<String>
        get() {
            val name = findViewById<EditText>(R.id.editTextTextPersonName)
            val phoneNumber = findViewById<EditText>(R.id.editTextPhone)
            val emailId = findViewById<EditText>(R.id.editTextTextEmailAddress)
            val bookName = findViewById<EditText>(R.id.editTextTextBookName)
            val t1 = "${getString(R.string.book_for)} ${name.text}"
            val t2: String = """
                ${getString(R.string.name_string)}: ${name.text}
                ${getString(R.string.email_text)}: ${emailId.text}
                ${getString(R.string.phone_number_short)}: ${phoneNumber.text}
                ${getString(R.string.book_name_text)}: ${bookName.text}
                ${getString(R.string.lend_duration)}: $DURATION
                ${getString(R.string.duration_text)}: 1 ${getString(R.string.lend_duration)}
                ${getString(R.string.rent)}: $RENT
            """.trimIndent()
            return arrayOf(t1, t2)
        }

    companion object {
        const val DURATION = 2.0 // Duration of lend
        const val RENT = 10.0 // Rent of book
        var requestMessage: String = ""
    }
}